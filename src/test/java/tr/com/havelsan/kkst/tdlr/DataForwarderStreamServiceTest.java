package tr.com.havelsan.kkst.tdlr;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.KStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.support.serializer.JsonSerde;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ConnectivityMatrixProp;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ICustomQueryService;
import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.ConnectivityMatrixEvent;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.TacticalModelEvent;
import tr.com.havelsan.kkst.tdlr.forwarder.DataForwarder;
import tr.com.havelsan.kkst.tdlr.service.DataForwarderStreamService;

import java.time.Duration;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static tr.com.havelsan.kkss.kafka.CustomKafkaTestUtils.close;
import static tr.com.havelsan.kkss.kafka.CustomKafkaTestUtils.toStream;
import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.*;

@ExtendWith(MockitoExtension.class)
public class DataForwarderStreamServiceTest {

    private final static String TACTICAL_MODEL_EVENT_CONSUMER = "tactical-model-event-topic";
    private final static String CONNECTIVITY_MATRIX_EVENT_CONSUMER = "connectivity-matrix-event-topic";
    private final static String CONNECTIVITY_MATRIX_EVENT_CONSUMER_TEST = "connectivity-matrix-event-test-topic";


    private KStream<Integer, DomainEventEnvelope<TacticalModelEvent<?>>> tacticalModelEventConsumer;
    private KStream<Integer, DomainEventEnvelope<ConnectivityMatrixEvent>> connectivityMatrixEventConsumer;


    private TopologyTestDriver topologyTestDriver;
    private StreamsBuilder streamsBuilder;


    @InjectMocks
    private DataForwarderStreamService dataForwarderStreamService;

    @InjectMocks
    private DataForwarder dataForwarder;

    @Mock(lenient = true)
    private ConnectivityMatrixProp connectivityMatrixProp;

    @Mock(lenient = true)
    private ICustomQueryService customQueryService;

    @BeforeEach
    void setUp() {

        given(connectivityMatrixProp.getName()).willReturn("dummy-connectivity-prop");
        given(customQueryService.get(anyString(), any())).willReturn(CONNECTIVITY_MATRIX_1);

        dataForwarderStreamService = new DataForwarderStreamService(
                connectivityMatrixProp,
                customQueryService,
                dataForwarder
        );

        streamsBuilder = new StreamsBuilder();

        tacticalModelEventConsumer = toStream(
                streamsBuilder,
                TACTICAL_MODEL_EVENT_CONSUMER,
                Serdes.Integer(),
                new JsonSerde<>(DomainEventEnvelope.class)
        );
        connectivityMatrixEventConsumer = toStream(
                streamsBuilder,
                CONNECTIVITY_MATRIX_EVENT_CONSUMER,
                Serdes.Integer(),
                new JsonSerde<>(DomainEventEnvelope.class)
        );


    }

    @AfterEach
    void tearDown() throws Exception {
        close(topologyTestDriver);
    }

    @Test
    void tacticalModelEventConsumerTestWithFlagTrue() {
        // given
        topologyTestDriver = createTopologyTestDriverForTacticalModelEventConsumer("TacticalModelEventConsumer-WithFlagIsTrue-test",
                true,
                dataForwarderStreamService,
                tacticalModelEventConsumer,
                streamsBuilder);

        // when
        sendDomainEventList(topologyTestDriver,
                TACTICAL_MODEL_EVENT_LIST_1,
                TACTICAL_MODEL_EVENT_CONSUMER);

        //then
        verifyExpectedData(dataForwarderStreamService.tacticalModelEventSupplier()
                .get(), EXPECTED_TACTICAL_MODEL_EVENT_LIST_1);


    }

    @Test
    void tacticalModelEventConsumerTestWithFlagFalse() {
        // given
        topologyTestDriver = createTopologyTestDriverForTacticalModelEventConsumer("TacticalModelEventConsumer-WithFlagIsFalse-test",
                false,
                dataForwarderStreamService,
                tacticalModelEventConsumer,
                streamsBuilder);

        // when
        sendDomainEventList(topologyTestDriver,
                TACTICAL_MODEL_EVENT_LIST_1,
                TACTICAL_MODEL_EVENT_CONSUMER);

        //then
        verifyExpectedData(dataForwarderStreamService.tacticalModelEventSupplier()
                .get().take(Duration.ofSeconds(1L)), EXPECTED_TACTICAL_MODEL_EVENT_LIST_EMPTY);


    }

    @Test
    void connectivityMatrixEventConsumerTest() {
        // given
        topologyTestDriver = createTopologyTestDriverForConnectivityMatrixEventConsumer("ConnectivityMatrixEventConsumer-test",
                dataForwarderStreamService,
                connectivityMatrixEventConsumer,
                streamsBuilder,
                CONNECTIVITY_MATRIX_EVENT_CONSUMER_TEST
        );

        // when
        sendDomainEventList(topologyTestDriver, CONNECTIVITY_MATRIX_EVENT_LIST_1, CONNECTIVITY_MATRIX_EVENT_CONSUMER);

        //then
        Map<Integer, DomainEventEnvelope> result = drainConnectivityMatrixConsumerEvent(topologyTestDriver,
                CONNECTIVITY_MATRIX_EVENT_CONSUMER_TEST);

        assertThat(dataForwarderStreamService.isStateStoreFlag())
                .isTrue();
        assertThat(result.keySet())
                .isEqualTo(EXPECTED_CONNECTIVITY_MATRIX_EVENT_MAP.keySet());


    }

    @Test
    void tacticalModelEventConsumerTestWitAvroSupplier() {
        // given
        topologyTestDriver = createTopologyTestDriverForTacticalModelEventConsumer("TacticalModelEventConsumer-WithAvroSupplier-test",
                true,
                dataForwarderStreamService,
                tacticalModelEventConsumer,
                streamsBuilder);

        // when
        sendDomainEventList(topologyTestDriver,
                TACTICAL_MODEL_EVENT_LIST_1,
                TACTICAL_MODEL_EVENT_CONSUMER);

        //then
        verifyExpectedData(dataForwarderStreamService.tacticalModelEventAvroSupplier()
                .get(), EXPECTED_TACTICAL_MODEL_EVENT_LIST_1);


    }

}

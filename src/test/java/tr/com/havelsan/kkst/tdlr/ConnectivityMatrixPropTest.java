package tr.com.havelsan.kkst.tdlr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ConnectivityMatrixInitiliazer;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ConnectivityMatrixProp;
import tr.com.havelsan.kkst.tdlr.dto.ConnectivityMatrix;

import static org.assertj.core.api.Assertions.assertThat;
import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.CONNECTIVITY_MATRIX_DUMMY;

public class ConnectivityMatrixPropTest {

    private ConnectivityMatrixProp connectivityMatrixProp;

    @BeforeEach
    void setUp() {
        connectivityMatrixProp = new ConnectivityMatrixProp();
    }

    @Test
    void nameTest() {
        connectivityMatrixProp.setName("TEST_NAME");

        assertThat(connectivityMatrixProp.getName()).isEqualTo("TEST_NAME");
    }
}

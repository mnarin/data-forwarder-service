package tr.com.havelsan.kkst.tdlr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ConnectivityMatrixAggregator;
import tr.com.havelsan.kkst.tdlr.dto.ConnectivityMatrix;

import static org.assertj.core.api.Assertions.assertThat;
import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.*;


public class ConnectivityAggregatorTest {


    private ConnectivityMatrixAggregator connectivityMatrixAggregator;


    @BeforeEach
    void setUp(){
        connectivityMatrixAggregator =new ConnectivityMatrixAggregator();
    }



    @Test
    void returnAggregatedWhenNotNullConnectivityMatrixIsGiven() {
        // When
        ConnectivityMatrix connectivityMatrixDto = connectivityMatrixAggregator.apply(1,CONNECTIVITY_MATRIX_1, CONNECTIVITY_MATRIX_DUMMY);

        // Then
        assertThat(CONNECTIVITY_MATRIX_1).isNotNull();
        assertThat(CONNECTIVITY_MATRIX_DUMMY).isNotNull();
        assertThat(connectivityMatrixDto).isEqualToComparingFieldByFieldRecursively(CONNECTIVITY_MATRIX_1);
    }

    @Test
    void returnAggregatedWhenNullConnectivityMatrixIsGiven() {
        // When
        ConnectivityMatrix connectivityMatrixDto = connectivityMatrixAggregator.apply(1, CONNECTIVITY_MATRIX_NULL, CONNECTIVITY_MATRIX_DUMMY);

        // Then
        assertThat(connectivityMatrixDto).isEqualToComparingFieldByFieldRecursively(CONNECTIVITY_MATRIX_DUMMY);
    }






}

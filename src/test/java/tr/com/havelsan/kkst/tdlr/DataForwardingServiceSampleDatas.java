package tr.com.havelsan.kkst.tdlr;


import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.test.TestUtils;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import tr.com.havelsan.kkss.kafka.CustomRecordUtil;
import tr.com.havelsan.kkss.kafka.CustomStreamsConfigBuilder;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ConnectivityMatrixProp;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ICustomQueryService;
import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelopeImpl;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.ConnectivityMatrixEvent;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.TacticalModelEvent;
import tr.com.havelsan.kkst.tdlr.datamodel.tacticaldata.track.AirTrack;
import tr.com.havelsan.kkst.tdlr.dto.ConnectivityMatrix;
import tr.com.havelsan.kkst.tdlr.enums.eventenums.DomainEventType;
import tr.com.havelsan.kkst.tdlr.enums.eventenums.RemoteLocalIndicator;
import tr.com.havelsan.kkst.tdlr.enums.technicalenums.ResourceTypesEnum;
import tr.com.havelsan.kkst.tdlr.service.DataForwarderStreamService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static tr.com.havelsan.kkss.kafka.CustomKafkaTestUtils.*;
import static tr.com.havelsan.kkss.kafka.CustomMapUtils.mkEntry;
import static tr.com.havelsan.kkss.kafka.CustomMapUtils.mkMap;

public class DataForwardingServiceSampleDatas {


    static ConnectivityMatrix CONNECTIVITY_MATRIX_1 = new ConnectivityMatrix();
    static ConnectivityMatrix CONNECTIVITY_MATRIX_DUMMY = new ConnectivityMatrix();
    static ConnectivityMatrix CONNECTIVITY_MATRIX_NULL = null;

    static Integer RESOURCE_ID_1 = 1;
    static Integer RESOURCE_ID_2 = 2;

    static List<Integer> CONNECTIVITY_LIST_1 = Collections.singletonList(RESOURCE_ID_2);


    static List<KeyValue<Integer, DomainEventEnvelope>> TACTICAL_MODEL_EVENT_LIST_1 = new ArrayList<>();
    static List<KeyValue<Integer, DomainEventEnvelope>> TACTICAL_MODEL_EVENT_LIST_DUMMY = new ArrayList<>();
    static List<KeyValue<Integer, DomainEventEnvelope>> CONNECTIVITY_MATRIX_EVENT_LIST_1 = new ArrayList<>();

    static List<DomainEventEnvelope<TacticalModelEvent<?>>> EXPECTED_TACTICAL_MODEL_EVENT_LIST_1 = new ArrayList<>();

    static List<DomainEventEnvelope<TacticalModelEvent<?>>> EXPECTED_TACTICAL_MODEL_EVENT_LIST_EMPTY = new ArrayList<>();
    static Map<Integer, DomainEventEnvelope> EXPECTED_TACTICAL_MODEL_EVENT_MAP_EMPTY = new HashMap<>();

    static AirTrack AIR_TRACK_1 = new AirTrack();

    static TacticalModelEvent<?> TACTICAL_MODEL_EVENT_1 = new TacticalModelEvent<>(1, AIR_TRACK_1, RemoteLocalIndicator.REMOTE);
    static TacticalModelEvent<?> TACTICAL_MODEL_EVENT_2 = new TacticalModelEvent<>(2, AIR_TRACK_1, RemoteLocalIndicator.LOCAL);
    static TacticalModelEvent<?> EXPECTED_TACTICAL_MODEL_EVENT_1 = new TacticalModelEvent<>(2, AIR_TRACK_1, RemoteLocalIndicator.LOCAL);

    static ConnectivityMatrixEvent CONNECTIVITY_MATRIX_EVENT_1 = new ConnectivityMatrixEvent(RESOURCE_ID_1, CONNECTIVITY_MATRIX_1);

    static DomainEventEnvelope DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT = new DomainEventEnvelopeImpl<>("1", DomainEventType.TACTICAL_MODEL_EVENT, TACTICAL_MODEL_EVENT_1);
    static DomainEventEnvelope DOMAIN_EVENT_ENVELOPE_DUMMY_FOR_TACTICAL_MODEL_EVENT = new DomainEventEnvelopeImpl<>("1", DomainEventType.TACTICAL_MODEL_CALCULATION_EVENT, TACTICAL_MODEL_EVENT_1);
    static DomainEventEnvelope DOMAIN_EVENT_ENVELOPE_2_FOR_TACTICAL_MODEL_EVENT = new DomainEventEnvelopeImpl<>("2", DomainEventType.TACTICAL_MODEL_CALCULATION_EVENT, TACTICAL_MODEL_EVENT_2);


    static DomainEventEnvelope DOMAIN_EVENT_ENVELOPE_1_FOR_CONNECTIVITY_MATRIX_EVENT = new DomainEventEnvelopeImpl<>("1", DomainEventType.CONNECTIVITY_MATRIX_EVENT, CONNECTIVITY_MATRIX_EVENT_1);

    static DomainEventEnvelope EXPECTED_DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT = new DomainEventEnvelopeImpl<>("2", DomainEventType.TACTICAL_MODEL_CALCULATION_EVENT, EXPECTED_TACTICAL_MODEL_EVENT_1);
    static DomainEventEnvelope EXPECTED_DOMAIN_EVENT_ENVELOPE_EMPTY = null;

    final static List<KeyValue<Integer, DomainEventEnvelope>> EXPECTED_TACTICAL_MODEL_EVENT_ENVELOPE_LIST = Stream.of(
            new KeyValue<>(RESOURCE_ID_1, DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT),
            new KeyValue<>(RESOURCE_ID_2, DOMAIN_EVENT_ENVELOPE_2_FOR_TACTICAL_MODEL_EVENT)
    ).collect(Collectors.toList());

    final static List<KeyValue<Integer, DomainEventEnvelope>> EXPECTED_TACTICAL_MODEL_EVENT_ENVELOPE_LIST_2 = Stream.of(
            new KeyValue<>(RESOURCE_ID_1, DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT)
    ).collect(Collectors.toList());

    static GenericRecord GENERIC_RECORD_2_FOR_TACTICAL_MODEL_EVENT = null;

    static {
        AIR_TRACK_1.getEntityKey().setResourceTypesEnum(ResourceTypesEnum.MIDS);
        Schema schemaForDomainEvent = CustomRecordUtil.generateSchema(DOMAIN_EVENT_ENVELOPE_2_FOR_TACTICAL_MODEL_EVENT.getClass());
        GENERIC_RECORD_2_FOR_TACTICAL_MODEL_EVENT = new GenericData.Record(schemaForDomainEvent);
        GENERIC_RECORD_2_FOR_TACTICAL_MODEL_EVENT = CustomRecordUtil
                .generateGenericRecord(
                        schemaForDomainEvent,
                        DOMAIN_EVENT_ENVELOPE_2_FOR_TACTICAL_MODEL_EVENT);


        fillExpectedTacticalModelEventList(EXPECTED_TACTICAL_MODEL_EVENT_LIST_1, EXPECTED_DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT);
        fillExpectedTacticalModelEventList(EXPECTED_TACTICAL_MODEL_EVENT_LIST_EMPTY, EXPECTED_DOMAIN_EVENT_ENVELOPE_EMPTY);
        fillTacticalModelEventList(TACTICAL_MODEL_EVENT_LIST_1, DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT, 1);
        fillTacticalModelEventList(TACTICAL_MODEL_EVENT_LIST_DUMMY, DOMAIN_EVENT_ENVELOPE_DUMMY_FOR_TACTICAL_MODEL_EVENT, 1);
        fillConnectivityMatrix(CONNECTIVITY_MATRIX_1, RESOURCE_ID_1, CONNECTIVITY_LIST_1);
        fillConnectivityMatrixEventList(CONNECTIVITY_MATRIX_EVENT_LIST_1, DOMAIN_EVENT_ENVELOPE_1_FOR_CONNECTIVITY_MATRIX_EVENT, RESOURCE_ID_1);


    }

    private static void fillConnectivityMatrixEventList(List<KeyValue<Integer, DomainEventEnvelope>> connectivityMatrixEventList, DomainEventEnvelope domainEventEnvelope1ForConnectivityMatrixEvent, Integer resourceId) {
        connectivityMatrixEventList.add(new KeyValue<>(resourceId, domainEventEnvelope1ForConnectivityMatrixEvent));
    }


    private static void fillConnectivityMatrix(ConnectivityMatrix connectivityMatrix, Integer resourceId, List<Integer> connectivityList) {
        connectivityMatrix.setSourceId(resourceId);
        connectivityMatrix.setConnectivities(connectivityList);
    }

    private static void fillTacticalModelEventList(List<KeyValue<Integer, DomainEventEnvelope>> tacticalModelEventList, DomainEventEnvelope domainEventEnvelope, Integer resourceId) {
        tacticalModelEventList.add(new KeyValue<>(resourceId, domainEventEnvelope));
    }


    private static void fillExpectedTacticalModelEventList(List<DomainEventEnvelope<TacticalModelEvent<?>>> expectedTacticalModelEventList, DomainEventEnvelope expectedDomainEventEnvelope) {
        Optional.ofNullable(expectedDomainEventEnvelope)
                .ifPresent(expectedTacticalModelEventList::add);

    }

    final static Map<Integer, DomainEventEnvelope> EXPECTED_CONNECTIVITY_MATRIX_EVENT_MAP = mkMap(
            mkEntry(RESOURCE_ID_1, DOMAIN_EVENT_ENVELOPE_1_FOR_CONNECTIVITY_MATRIX_EVENT)
    );
    final static Map<Integer, DomainEventEnvelope> EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE = mkMap(
            mkEntry(RESOURCE_ID_1, DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT),
            mkEntry(RESOURCE_ID_2, DOMAIN_EVENT_ENVELOPE_2_FOR_TACTICAL_MODEL_EVENT)
    );
    final static Map<Integer, GenericRecord> EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE_FOR_AVTO = mkMap(
            mkEntry(RESOURCE_ID_2, GENERIC_RECORD_2_FOR_TACTICAL_MODEL_EVENT)
    );
    final static Map<Integer, DomainEventEnvelope> EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FALSE = mkMap(
            mkEntry(RESOURCE_ID_1, DOMAIN_EVENT_ENVELOPE_1_FOR_TACTICAL_MODEL_EVENT)
    );
    final static Map<Integer, DomainEventEnvelope> EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITHOUT_FORWARDED =
            EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FALSE;
    final static Map<Integer, DomainEventEnvelope> EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FORWARDED =
            EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE;

    public static void verifyExpectedData(Flux<Message<DomainEventEnvelope<TacticalModelEvent<?>>>> messageFlux, List<DomainEventEnvelope<TacticalModelEvent<?>>> expectedTacticalModelEventList) {
        StepVerifier.create(messageFlux
                .map(Message::getPayload))
                .recordWith(ArrayList::new)
                .expectNextCount(expectedTacticalModelEventList.size())
                .consumeRecordedWith(results -> {
                    assertThat(results)
                            .isNotNull()
                            .usingRecursiveFieldByFieldElementComparator()
                            .containsExactlyInAnyOrderElementsOf(expectedTacticalModelEventList);
                })
                .thenCancel()
                .verify();
    }

    public static Map<Integer, DomainEventEnvelope> drainConnectivityMatrixConsumerEvent(TopologyTestDriver topologyTestDriver, String topic) {
        return drainTableOutput(
                topic,
                topologyTestDriver,
                Serdes.Integer().deserializer(),
                new JsonSerde<>(DomainEventEnvelope.class).deserializer()
        );
    }


    public static TopologyTestDriver createTopologyTestDriverForConnectivityMatrixEventConsumer(String applicationId,
                                                                                                DataForwarderStreamService dataForwarderStreamService,
                                                                                                KStream<Integer, DomainEventEnvelope<ConnectivityMatrixEvent>> connectivityMatrixEventConsumer,
                                                                                                StreamsBuilder streamsBuilder,
                                                                                                String topic) {
        dataForwarderStreamService
                .connectivityMatrixEventConsumer()
                .accept(connectivityMatrixEventConsumer);
        connectivityMatrixEventConsumer.to(topic);
        Properties properties = getProperties(applicationId);
        return topologyTestDriver(streamsBuilder, properties);
    }

    public static TopologyTestDriver createTopologyTestDriverForTacticalModelEventConsumer(String applicationId,
                                                                                           Boolean flag,
                                                                                           DataForwarderStreamService dataForwarderStreamService,
                                                                                           KStream<Integer, DomainEventEnvelope<TacticalModelEvent<?>>> tacticalModelEventConsumer,
                                                                                           StreamsBuilder streamsBuilder) {
        dataForwarderStreamService.setStateStoreFlag(flag);
        dataForwarderStreamService
                .tacticalModelEventConsumer()
                .accept(tacticalModelEventConsumer);

        Properties properties = getProperties(applicationId);
        return topologyTestDriver(streamsBuilder, properties);
    }

    public static void sendDomainEventList(TopologyTestDriver topologyTestDriver, List<KeyValue<Integer, DomainEventEnvelope>> createEventList, String topic) {
        produceKeyValuesSynchronously(
                topic,
                createEventList,
                topologyTestDriver,
                Serdes.Integer().serializer(),
                new JsonSerde<>(DomainEventEnvelope.class).serializer()
        );
    }

    public static ICustomQueryService getInteractiveCustomQuery(ConnectivityMatrixProp props) {
        Map<String, Map<Object, Object>> map = new HashMap<>();
        HashMap<Object, Object> internalMap = new HashMap<>();
        internalMap.put(RESOURCE_ID_1, CONNECTIVITY_MATRIX_1);
        map.put(props.getName(), internalMap);
        return new CucumberCustomKafkaInteractiveQuery(map);
    }

    private static <V> Properties getProperties(String applicationId) {
        Properties properties = new Properties();
        properties.putAll(new CustomStreamsConfigBuilder()
                .applicationId(applicationId)
                .bootstrapServers("dummy config")
                .autoOffset("earliest")
                .defaultKeySerde(Serdes.String().getClass())
                .defaultValueSerde(JsonSerde.class)
                .stateDirectoryConfig(TestUtils.tempDirectory().getAbsolutePath())
                .build());
        return properties;
    }
}
package tr.com.havelsan.kkst.tdlr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ConnectivityMatrixInitiliazer;
import tr.com.havelsan.kkst.tdlr.dto.ConnectivityMatrix;

import static org.assertj.core.api.Assertions.assertThat;
import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.CONNECTIVITY_MATRIX_DUMMY;

public class ConnectivityMatrixInitializerTest {

    private ConnectivityMatrixInitiliazer connectivityMatrixInitiliazer;

    @BeforeEach
    void setUp() {
        connectivityMatrixInitiliazer = new ConnectivityMatrixInitiliazer();
    }

    @Test
    void instantiateNetworkDto() {
        ConnectivityMatrix connectivityMatrixDto = connectivityMatrixInitiliazer.apply();

        assertThat(connectivityMatrixDto).isEqualToComparingFieldByFieldRecursively(CONNECTIVITY_MATRIX_DUMMY);
    }

}

package tr.com.havelsan.kkst.tdlr.connectivitymatrix;

import org.apache.kafka.streams.kstream.Initializer;
import tr.com.havelsan.kkst.tdlr.dto.ConnectivityMatrix;

public class ConnectivityMatrixInitiliazer implements Initializer<ConnectivityMatrix> {
    @Override
    public ConnectivityMatrix apply() {
        return new ConnectivityMatrix();
    }
}

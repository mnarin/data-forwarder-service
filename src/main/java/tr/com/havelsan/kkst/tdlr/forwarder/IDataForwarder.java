package tr.com.havelsan.kkst.tdlr.forwarder;

import org.apache.kafka.streams.KeyValue;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.TacticalModelEvent;


public interface IDataForwarder {
    KeyValue<Integer, TacticalModelEvent<?>> forward(TacticalModelEvent<?> tacticalModelEvent, Integer connectivityMatrixDto);
}

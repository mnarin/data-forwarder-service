package tr.com.havelsan.kkst.tdlr.connectivitymatrix;

import org.apache.kafka.streams.kstream.Aggregator;
import tr.com.havelsan.kkst.tdlr.dto.ConnectivityMatrix;

import java.util.Optional;

public class ConnectivityMatrixAggregator implements Aggregator<Integer, ConnectivityMatrix, ConnectivityMatrix> {
    @Override
    public ConnectivityMatrix apply(Integer integer, ConnectivityMatrix newConnectivityMatrix, ConnectivityMatrix oldConnectivityMatrix2) {
        return Optional.ofNullable(newConnectivityMatrix)
                .orElse(oldConnectivityMatrix2);
    }
}

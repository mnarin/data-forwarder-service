package tr.com.havelsan.kkst.tdlr.connectivitymatrix;

public interface ICustomQueryService {
    <K, V> V get(String storeName, K key);
}

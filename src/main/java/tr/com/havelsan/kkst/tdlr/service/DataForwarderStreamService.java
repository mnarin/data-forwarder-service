package tr.com.havelsan.kkst.tdlr.service;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;
import tr.com.havelsan.kkss.kafka.MaterializedBuilder;
import tr.com.havelsan.kkss.kafka.Materializer;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.*;
import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.ConnectivityMatrixEvent;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.TacticalModelEvent;
import tr.com.havelsan.kkst.tdlr.dto.ConnectivityMatrix;
import tr.com.havelsan.kkst.tdlr.enums.eventenums.RemoteLocalIndicator;
import tr.com.havelsan.kkst.tdlr.forwarder.IDataForwarder;
import tr.com.havelsan.kkst.tdlr.util.event.DomainEventEnvelopeUtil;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class DataForwarderStreamService implements IDataForwarderStreamService {

    private final IDataForwarder dataForwarder;
    private Map<Integer, ConnectivityMatrix> connectivityServiceMap;
    private final ICustomQueryService customQueryService;
    private final ConnectivityMatrixProp connectivityMatrixProp;

    private final EmitterProcessor<Message<DomainEventEnvelope<TacticalModelEvent<?>>>> tacticalModels;

    private boolean stateStoreFlag;

    public DataForwarderStreamService(ConnectivityMatrixProp connectivityMatrixProp,
                                      ICustomQueryService customQueryService,
                                      IDataForwarder dataForwarder) {
        this.customQueryService = customQueryService;
        this.dataForwarder = dataForwarder;
        this.connectivityMatrixProp = connectivityMatrixProp;

        this.tacticalModels = EmitterProcessor.create();
    }

    @Override
    @Bean
    public Consumer<KStream<Integer, DomainEventEnvelope<TacticalModelEvent<?>>>> tacticalModelEventConsumer() {
        return (tacticalModelKStream) -> tacticalModelKStream
                .mapValues(DomainEventEnvelopeUtil::extractTacticalModelEvent)
                .filter((integer, optionalEvent) -> optionalEvent.isPresent())
                .mapValues(Optional::get)
                .filter(RemoteLocalIndicator.REMOTE::isThisType)
                .flatMap((resourceId, tacticalModelEvent) -> getConnectivityMatrix(resourceId)
                        .getConnectivities()
                        .stream()
                        .map(id -> dataForwarder.forward(tacticalModelEvent, id))
                        .collect(Collectors.toList())
                )
                .mapValues(DomainEventEnvelopeUtil::envelopTacticalModelCalculationEvent)
                .mapValues((resourceId, eventEnvelope) -> DomainEventEnvelopeUtil.createMessageWithHeader(
                        KafkaHeaders.MESSAGE_KEY,
                        resourceId,
                        eventEnvelope))
                .foreach((integer, envelopeMessage) -> this.tacticalModels.onNext(envelopeMessage));
    }

    @Override
    @Bean
    public Consumer<KStream<Integer, DomainEventEnvelope<ConnectivityMatrixEvent>>> connectivityMatrixEventConsumer() {
        return connectivityMatrixKStream -> {
            Materializer<Integer, ConnectivityMatrix> materializer = new MaterializedBuilder<>(Serdes.Integer(), new JsonSerde<>(ConnectivityMatrix.class))
                    .createMaterialized(connectivityMatrixProp.getName());


            connectivityMatrixKStream
                    .mapValues(DomainEventEnvelopeUtil::extractConnectivityMatrixEvent)
                    .filter((integer, optionalEvent) -> optionalEvent.isPresent())
                    .mapValues(Optional::get)
                    .mapValues(ConnectivityMatrixEvent::getConnectivityMatrix)
                    .mapValues((integer, connectivityMatrix) -> {
                        this.setStateStoreFlag(true);
                        return connectivityMatrix;
                    })
                    .groupByKey()
                    .aggregate(
                            new ConnectivityMatrixInitiliazer(),
                            new ConnectivityMatrixAggregator(),
                            materializer.get()
                    );

        };
    }

    @Override
    @Bean
    public Supplier<Flux<Message<DomainEventEnvelope<TacticalModelEvent<?>>>>> tacticalModelEventSupplier() {
        return () -> tacticalModels;
    }

    @Override
    @Bean
    public Supplier<Flux<Message<DomainEventEnvelope<TacticalModelEvent<?>>>>> tacticalModelEventAvroSupplier() {
        return () -> tacticalModels;
    }


    private ConnectivityMatrix getConnectivityMatrix(Integer key) {
        return Optional.ofNullable(key)
                .filter((x) -> isStateStoreFlag())
                .map(k -> customQueryService
                        .<Integer, ConnectivityMatrix>get(connectivityMatrixProp.getName(), k))
                .orElse(new ConnectivityMatrix());
    }


    public boolean isStateStoreFlag() {
        return stateStoreFlag;
    }

    public void setStateStoreFlag(boolean stateStoreFlag) {
        this.stateStoreFlag = stateStoreFlag;
    }
}

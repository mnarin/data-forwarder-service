package tr.com.havelsan.kkst.tdlr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataForwarderApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataForwarderApplication.class, args);
    }
}

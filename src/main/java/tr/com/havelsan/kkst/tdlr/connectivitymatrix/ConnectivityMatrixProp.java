package tr.com.havelsan.kkst.tdlr.connectivitymatrix;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "connectivity.store")
public class ConnectivityMatrixProp {

    public static final String DEFAULT_CONNECTIVITY_STORE_NAME = "default-connectivity-store";

    private String name = DEFAULT_CONNECTIVITY_STORE_NAME;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

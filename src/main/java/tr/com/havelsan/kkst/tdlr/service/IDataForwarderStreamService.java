package tr.com.havelsan.kkst.tdlr.service;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.ConnectivityMatrixEvent;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.TacticalModelEvent;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created by nozdemir on 7.10.2020.
 */
public interface IDataForwarderStreamService {
    @Bean
    Consumer<KStream<Integer, DomainEventEnvelope<TacticalModelEvent<?>>>> tacticalModelEventConsumer();

    @Bean
    Consumer<KStream<Integer, DomainEventEnvelope<ConnectivityMatrixEvent>>> connectivityMatrixEventConsumer();

    @Bean
    Supplier<Flux<Message<DomainEventEnvelope<TacticalModelEvent<?>>>>> tacticalModelEventSupplier();

    @Bean
    Supplier<Flux<Message<DomainEventEnvelope<TacticalModelEvent<?>>>>> tacticalModelEventAvroSupplier();
}

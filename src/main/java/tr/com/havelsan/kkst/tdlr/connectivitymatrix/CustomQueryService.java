package tr.com.havelsan.kkst.tdlr.connectivitymatrix;

import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.stereotype.Component;

@Component
public class CustomQueryService implements ICustomQueryService {
    private final InteractiveQueryService interactiveQueryService;

    public CustomQueryService(InteractiveQueryService interactiveQueryService) {
        this.interactiveQueryService = interactiveQueryService;
    }

    public <K, V> V get(String storeName, K key) {
        return interactiveQueryService
                .getQueryableStore(storeName, QueryableStoreTypes.<K, V>keyValueStore())
                .get(key);
    }
}
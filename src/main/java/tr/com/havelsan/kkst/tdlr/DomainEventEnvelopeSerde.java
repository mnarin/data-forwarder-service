package tr.com.havelsan.kkst.tdlr;


import org.springframework.kafka.support.serializer.JsonSerde;
import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;

public class DomainEventEnvelopeSerde extends JsonSerde<DomainEventEnvelope> {

}

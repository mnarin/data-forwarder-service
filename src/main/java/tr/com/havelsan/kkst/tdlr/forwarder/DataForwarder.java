package tr.com.havelsan.kkst.tdlr.forwarder;

import org.apache.kafka.streams.KeyValue;
import org.springframework.stereotype.Service;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.TacticalModelEvent;
import tr.com.havelsan.kkst.tdlr.enums.eventenums.RemoteLocalIndicator;


@Service
public class DataForwarder implements IDataForwarder {
    @Override
    public KeyValue<Integer, TacticalModelEvent<?>> forward(TacticalModelEvent<?> baseTacticalModel,
                                                            Integer destinationResourceId) {
        baseTacticalModel.setRemoteLocalIndicator(RemoteLocalIndicator.LOCAL);
        baseTacticalModel.setResourceId(destinationResourceId);

        return new KeyValue<>(destinationResourceId, baseTacticalModel);
    }
}

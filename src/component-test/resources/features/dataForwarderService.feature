Feature: DF Service

  Scenario Outline: Data Forwarder Calculations
    Given a valid <event-type> and connectivity matrix.
    When it arrives <event-type>.
    Then it should be happened that <action> action and <forwarding-status>.


    Examples: TACTICAL MODEL TOPIC AND CONNECTIVITY MATRIX CALCULATION
      | event-type                | action                               | forwarding-status |
      | TACTICAL_MODEL_EVENT      | TACTICAL_MODEL_CALCULATION_EVENT     | FORWARDED         |
      | CONNECTIVITY_MATRIX_EVENT | CREATE_OR_UPDATE_CONNECTIVITY_MATRIX | NOT_FORWARDED     |
      | ALL_OTHER_EVENT           | NOT_GENERATE_EVENT                   | NOT_FORWARDED     |


package tr.com.havelsan.kkst.tdlr;

import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ICustomQueryService;

import java.util.Map;

public class CucumberCustomKafkaInteractiveQuery implements ICustomQueryService {
    private Map<String, Map<Object, Object>> map;

    public CucumberCustomKafkaInteractiveQuery(Map<String, Map<Object, Object>> map) {
        this.map = map;
    }

    @Override
    public <K, V> V get(String storeName, K key) {
        return (V) map.get(storeName).get(key);
    }
}

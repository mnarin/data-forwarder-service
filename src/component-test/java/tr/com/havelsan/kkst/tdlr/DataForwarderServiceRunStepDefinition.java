package tr.com.havelsan.kkst.tdlr;


import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.KStream;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.support.serializer.JsonSerde;
import tr.com.havelsan.kkst.tdlr.connectivitymatrix.ConnectivityMatrixProp;
import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.ConnectivityMatrixEvent;
import tr.com.havelsan.kkst.tdlr.datamodel.eventdata.TacticalModelEvent;
import tr.com.havelsan.kkst.tdlr.forwarder.DataForwarder;
import tr.com.havelsan.kkst.tdlr.forwarder.IDataForwarder;
import tr.com.havelsan.kkst.tdlr.service.DataForwarderStreamService;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static tr.com.havelsan.kkss.kafka.CustomKafkaTestUtils.close;
import static tr.com.havelsan.kkss.kafka.CustomKafkaTestUtils.toStream;
import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.*;

@ExtendWith(MockitoExtension.class)
public class DataForwarderServiceRunStepDefinition {
    private final static String TACTICAL_MODEL_EVENT_CONSUMER = "tactical-model-event-topic";
    private final static String CONNECTIVITY_MATRIX_EVENT_CONSUMER = "connectivity-matrix-event-topic";
    private final static String CONNECTIVITY_MATRIX_EVENT_CONSUMER_TEST = "connectivity-matrix-event-test-topic";


    private KStream<Integer, DomainEventEnvelope<TacticalModelEvent<?>>> tacticalModelEventConsumer;
    private KStream<Integer, DomainEventEnvelope<ConnectivityMatrixEvent>> connectivityMatrixEventConsumer;
    private List<KeyValue<Integer, DomainEventEnvelope>> GIVEN_DATA;
    private DataForwarderStreamService dataForwarderStreamService;

    private TopologyTestDriver topologyTestDriver;


    private StreamsBuilder streamsBuilder;


    private IDataForwarder dataForwarder = new DataForwarder();


    private ConnectivityMatrixProp connectivityMatrixProp = new ConnectivityMatrixProp();


    @Before
    public void setUp(Scenario scenario) {

        String scenarioId = scenario.getId().replaceAll("[/;]", "-");

        streamsBuilder = new StreamsBuilder();

        dataForwarderStreamService = new DataForwarderStreamService(
                connectivityMatrixProp,
                getInteractiveCustomQuery(connectivityMatrixProp),
                dataForwarder
        );

        tacticalModelEventConsumer = toStream(
                streamsBuilder,
                TACTICAL_MODEL_EVENT_CONSUMER,
                Serdes.Integer(),
                new JsonSerde<>(DomainEventEnvelope.class)
        );
        connectivityMatrixEventConsumer = toStream(
                streamsBuilder,
                CONNECTIVITY_MATRIX_EVENT_CONSUMER,
                Serdes.Integer(),
                new JsonSerde<>(DomainEventEnvelope.class)
        );

        topologyTestDriver = createTopologyTestDriverForTacticalModelEventConsumer(scenarioId, true, dataForwarderStreamService, tacticalModelEventConsumer, streamsBuilder);
    }

    @After
    public void tearDown() throws Exception {
        close(topologyTestDriver);
        GIVEN_DATA.clear();
    }

    @Given("a valid (.*) and connectivity matrix.")
    public void useValidEventType(String eventType
    ) {

        switch (eventType) {
            case "TACTICAL_MODEL_EVENT":
                GIVEN_DATA = TACTICAL_MODEL_EVENT_LIST_1;
                break;
            case "CONNECTIVITY_MATRIX_EVENT":
                GIVEN_DATA = CONNECTIVITY_MATRIX_EVENT_LIST_1;
                break;
            default:
                GIVEN_DATA = TACTICAL_MODEL_EVENT_LIST_DUMMY;
                break;
        }

    }

    @When("it arrives (.*).")
    public void arrivedTEvent(String eventType) {
        switch (eventType) {
            case "CONNECTIVITY_MATRIX_EVENT":
                // when
                sendDomainEventList(topologyTestDriver, CONNECTIVITY_MATRIX_EVENT_LIST_1, CONNECTIVITY_MATRIX_EVENT_CONSUMER);
                break;
            default:
                sendDomainEventList(topologyTestDriver, GIVEN_DATA, TACTICAL_MODEL_EVENT_CONSUMER);
                break;
        }
    }

    @Then("it should be happened that (.*) action and (.*).")
    public void isCreatedParticipantObject(String eventType, String forwardingStatus) {
        switch (eventType) {
            case "TACTICAL_MODEL_CALCULATION_EVENT":
                verifyExpectedData(dataForwarderStreamService.tacticalModelEventSupplier()
                        .get(), EXPECTED_TACTICAL_MODEL_EVENT_LIST_1);
                assertThat(forwardingStatus)
                        .isEqualTo("FORWARDED");
                break;
            case "CONNECTIVITY_MATRIX_EVENT":
                Map<Integer, DomainEventEnvelope> result = drainConnectivityMatrixConsumerEvent(topologyTestDriver, CONNECTIVITY_MATRIX_EVENT_CONSUMER_TEST);

                assertThat(dataForwarderStreamService.isStateStoreFlag())
                        .isTrue();
                assertThat(result.keySet())
                        .isEqualTo(EXPECTED_CONNECTIVITY_MATRIX_EVENT_MAP.keySet());
                assertThat(forwardingStatus)
                        .isEqualTo("NOT_FORWARDED");
                break;
            default:
                verifyExpectedData(dataForwarderStreamService.tacticalModelEventSupplier()
                        .get().take(Duration.ofSeconds(1L)), EXPECTED_TACTICAL_MODEL_EVENT_LIST_EMPTY);
                assertThat(forwardingStatus)
                        .isEqualTo("NOT_FORWARDED");
                break;
        }


    }


}

package tr.com.havelsan.kkst.tdlr;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features/dataForwarderService.feature")
public class DataForwarderServiceComponentTest {

}

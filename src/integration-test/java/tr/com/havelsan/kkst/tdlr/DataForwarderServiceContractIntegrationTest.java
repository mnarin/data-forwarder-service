//package tr.com.havelsan.kkst.tdlr;
//
//import org.apache.kafka.clients.consumer.Consumer;
//import org.apache.kafka.common.serialization.Serdes;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.cloud.contract.stubrunner.StubFinder;
//import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
//import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
//import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
//import org.springframework.context.annotation.Import;
//import org.springframework.kafka.support.serializer.JsonSerde;
//import org.springframework.kafka.test.EmbeddedKafkaBroker;
//import org.springframework.kafka.test.context.EmbeddedKafka;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//import tr.com.havelsan.kkss.kafka.contract.CustomContractVerifierKafkaConfiguration;
//import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
//
//import java.util.Map;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static tr.com.havelsan.kkss.kafka.CustomSpringKafkaTestUtils.consumer;
//import static tr.com.havelsan.kkss.kafka.CustomSpringKafkaTestUtils.drainTableOutput;
//import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITHOUT_FORWARDED;
//import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FORWARDED;
//
///**
// * Created by nozdemir on 21.10.2020.
// */
//@SpringBootTest(classes = DataForwarderApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE,
//        properties = {
//                "server.port=0",
//                "spring.jmx.enabled=false",
//                "spring.cloud.stream.function.definition=tacticalModelEventConsumer;tacticalModelEventSupplier;connectivityMatrixEventConsumer;",
//                "spring.cloud.stream.bindings.tacticalModelEventConsumer-in-0.destination=" + DataForwarderServiceContractIntegrationTest.TACTICAL_MODEL_TOPIC,
//                "spring.cloud.stream.bindings.connectivityMatrixEventConsumer-in-0.destination=" + DataForwarderServiceContractIntegrationTest.CONNECTIVITY_MATRIX_TOPIC,
//                "spring.cloud.stream.bindings.tacticalModelEventSupplier-out-0.destination=" + DataForwarderServiceContractIntegrationTest.TACTICAL_MODEL_TOPIC,
//                "spring.kafka.consumer.group-id=data-forwarding-service-group-test",
//                "spring.kafka.producer.bootstrap-servers=127.0.0.1:9092",
//                "spring.kafka.producer.key-serializer=org.apache.kafka.common.serialization.IntegerSerializer",
//                "spring.kafka.producer.value-serializer=org.apache.kafka.common.serialization.StringSerializer",
//                "spring.cloud.stream.kafka.binder.brokers=127.0.0.1:9092",
//                "spring.cloud.stream.kafka.streams.binder.application-id=data-forwarding-service-app-id-test",
//                "spring.cloud.stream.kafka.bindings.tacticalModelEventSupplier-out-0.producer.configuration.key.serializer=org.apache.kafka.common.serialization.IntegerSerializer",
//                "spring.cloud.stream.kafka.bindings.tacticalModelEventSupplier-out-0.producer.configuration.value.serializer=org.apache.kafka.common.serialization.ByteArraySerializer",
//                "spring.cloud.stream.kafka.streams.bindings.tacticalModelEventConsumer-in-0.consumer.application-id=tactical-model-input-df-id-test",
//                "spring.cloud.stream.kafka.streams.bindings.connectivityMatrixEventConsumer-in-0.consumer.application-id=connectivity-matrix-input-df-id-test",
//                "stubrunner.kafka.enabled=true",
//                "stubrunner.stream.enabled=false",
//                "stubrunner.integration.enabled=false"
//        }
//)
//@AutoConfigureMessageVerifier
//@SpringJUnitConfig
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//@EmbeddedKafka(
//        partitions = 1,
//        topics = {
//                DataForwarderServiceContractIntegrationTest.TACTICAL_MODEL_TOPIC,
//                DataForwarderServiceContractIntegrationTest.TACTICAL_MODEL_AVRO_TOPIC,
//                DataForwarderServiceContractIntegrationTest.CONNECTIVITY_MATRIX_TOPIC
//        }
//        , brokerProperties = {"listeners=PLAINTEXT://127.0.0.1:9092", "port=9092"}
//
//)
//@AutoConfigureStubRunner(
//        repositoryRoot = "${stubrunner.properties.repository-root}",
//        stubsMode = StubRunnerProperties.StubsMode.REMOTE,
//        ids = {
//                "tr.com.havelsan.kkst.tdlr:mission-system-interface-service",
//                "tr.com.havelsan.kkst.tdlr:tactical-message-processor-service"
//        }
//)
//@Import({CustomContractVerifierKafkaConfiguration.class})
//public class DataForwarderServiceContractIntegrationTest {
//    static final String TACTICAL_MODEL_TOPIC = "tactical-model-topic";
//    static final String TACTICAL_MODEL_AVRO_TOPIC = "tactical-model-avro-topic";
//    static final String CONNECTIVITY_MATRIX_TOPIC = "connectivity-matrix-topic";
//
//    @Autowired
//    StubFinder stubFinder;
//
//    @Autowired
//    private EmbeddedKafkaBroker embeddedKafka;
//
//    private Consumer<Integer, DomainEventEnvelope> tacticalModelEventConsumer;
//
//    @BeforeEach
//    void setUp() {
//        tacticalModelEventConsumer = consumer(
//                TACTICAL_MODEL_TOPIC,
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                embeddedKafka
//        );
//    }
//
//    @Test
//    void returnNoEmittedCalculationTacticalModelEventWhenTacticalModelEventIsTriggeredWithoutConnectivityMatrixEvent() throws InterruptedException {
//        // When
//        this.stubFinder.trigger("tacticalModelEvent");
//        Thread.sleep(5000L);
//
//        // Then
//        Map<Integer, DomainEventEnvelope> notForwardedTacticalModelEventMap = drainTableOutput(tacticalModelEventConsumer);
//
//        assertThat(notForwardedTacticalModelEventMap)
//                .hasSize(1)
//                .usingRecursiveComparison()
//                .ignoringFields("event.baseTacticalModel.entityKey.key", "event.baseTacticalModel.entityKey.resourceTypesEnum")
//                .isEqualTo(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITHOUT_FORWARDED);
//    }
//
//    @Test
//    void returnEmittedCalculationTacticalModelEventWhenTacticalModelEventIsTriggeredWithConnectivityMatrixEvent() throws InterruptedException {
//        // When
//        this.stubFinder.trigger("connectivityMatrixEvent");
//        Thread.sleep(5000L);
//        this.stubFinder.trigger("tacticalModelEvent");
//        Thread.sleep(5000L);
//
//        // Then
//        Map<Integer, DomainEventEnvelope> forwardedTacticalModelEventMap = drainTableOutput(tacticalModelEventConsumer);
//
//        assertThat(forwardedTacticalModelEventMap)
//                .hasSize(2)
//                .usingRecursiveComparison()
//                .ignoringFields("event.baseTacticalModel.entityKey.key", "event.baseTacticalModel.entityKey.resourceTypesEnum")
//                .isEqualTo(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FORWARDED);
//    }
//}

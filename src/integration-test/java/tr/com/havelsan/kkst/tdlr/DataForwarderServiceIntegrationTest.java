//package tr.com.havelsan.kkst.tdlr;
//
//import org.apache.kafka.clients.consumer.Consumer;
//import org.apache.kafka.common.serialization.Serdes;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.WebApplicationType;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.serializer.JsonSerde;
//import org.springframework.kafka.test.EmbeddedKafkaBroker;
//import org.springframework.kafka.test.context.EmbeddedKafka;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
//
//import java.util.Map;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static tr.com.havelsan.kkss.kafka.CustomSpringKafkaTestUtils.*;
//import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.*;
//
//
//@SpringJUnitConfig
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//@EmbeddedKafka(
//        partitions = 1,
//        topics = {
//                DataForwarderServiceIntegrationTest.TACTICAL_MODEL_EVENT_CONSUMER,
//                DataForwarderServiceIntegrationTest.CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                DataForwarderServiceIntegrationTest.TACTICAL_MODEL_EVENT_SUPPLIER,
//                DataForwarderServiceIntegrationTest.TACTICAL_MODEL_EVENT_AVRO_SUPPLIER,
//        }
////        brokerProperties = {"transactionTopicReplicationFactor=1", "transactionTopicMinISR=1"}
////        , brokerProperties = {"transaction.state.log.replication.factor=1", "transaction.state.log.min.isr=1"}
//)
//class DataForwarderServiceIntegrationTest {
//    static final String TACTICAL_MODEL_EVENT_CONSUMER = "tactical-model-topic";
//    static final String CONNECTIVITY_MATRIX_EVENT_CONSUMER = "connectivity-matrix-topic";
//    static final String TACTICAL_MODEL_EVENT_SUPPLIER = "tactical-model-topic";
//    static final String TACTICAL_MODEL_EVENT_AVRO_SUPPLIER = "tactical-model-avro-topic";
//
//
//    @Autowired
//    private EmbeddedKafkaBroker embeddedKafka;
//    private KafkaTemplate<Integer, DomainEventEnvelope> tacticalModelEventKafkaTemplate;
//    private KafkaTemplate<Integer, DomainEventEnvelope> connectivityMatrixEventKafkaTemplate;
//    private KafkaTemplate<Integer, DomainEventEnvelope> senderKafkaTemplateTacticalModelEvent;
//
//    private Consumer<Integer, DomainEventEnvelope> consumerTacticalModelEvent;
//    private Consumer<Integer, DomainEventEnvelope> consumerConnectivityMatrixEvent;
//
//    @BeforeEach
//    void setUp() {
//
//        connectivityMatrixEventKafkaTemplate = producer(
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                embeddedKafka
//        );
//        tacticalModelEventKafkaTemplate = producer(
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                embeddedKafka
//        );
//        consumerTacticalModelEvent = consumer(
//                TACTICAL_MODEL_EVENT_CONSUMER,
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                embeddedKafka
//        );
//        consumerConnectivityMatrixEvent = consumer(
//                CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                embeddedKafka
//        );
//
//
//    }
//
//    @AfterEach
//    void tearDown() {
//        close(consumerTacticalModelEvent);
//        close(consumerConnectivityMatrixEvent);
//
//    }
//
//    @Test
//    void emitTacticalModelEventConsumerWithStateStoreFlagIsFalseTest() {
//        try (ConfigurableApplicationContext ignored = startApplication()) {
//            // when
//            produceKeyValuesSynchronously(
//                    tacticalModelEventKafkaTemplate,
//                    TACTICAL_MODEL_EVENT_CONSUMER,
//                    TACTICAL_MODEL_EVENT_LIST_1.stream()
//            );
//            Map<Integer, DomainEventEnvelope> tacticalModelEventResult = drainTableOutput(consumerTacticalModelEvent);
//
//            // then
//            assertThat(tacticalModelEventResult.keySet())
//                    .isEqualTo(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FALSE.keySet());
//            assertThat(tacticalModelEventResult).isEqualToComparingFieldByFieldRecursively(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FALSE);
//
//        }
//    }
//
//    @Test
//    void emitTacticalModelEventConsumerWithStateStoreFlagIsTrueTest() {
//        try (ConfigurableApplicationContext ignored = startApplication()) {
//            //given
//
//            produceKeyValuesSynchronously(
//                    connectivityMatrixEventKafkaTemplate,
//                    CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                    CONNECTIVITY_MATRIX_EVENT_LIST_1.stream()
//            );
//            Thread.sleep(5000L);
//            // when
//            produceKeyValuesSynchronously(
//                    tacticalModelEventKafkaTemplate,
//                    TACTICAL_MODEL_EVENT_CONSUMER,
//                    TACTICAL_MODEL_EVENT_LIST_1.stream()
//            );
//            Thread.sleep(5000L);
//            Map<Integer, DomainEventEnvelope> tacticalModelEventResult = drainTableOutput(consumerTacticalModelEvent);
//
//            // then
//            assertThat(tacticalModelEventResult.keySet())
//                    .isEqualTo(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE.keySet());
//            assertThat(tacticalModelEventResult).isEqualToComparingFieldByFieldRecursively(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE);
//
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    void emitConnectivityMatrixEventConsumerTest() {
//        try (ConfigurableApplicationContext ignored = startApplication()) {
//            // when
//            produceKeyValuesSynchronously(
//                    connectivityMatrixEventKafkaTemplate,
//                    CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                    CONNECTIVITY_MATRIX_EVENT_LIST_1.stream()
//            );
//            Map<Integer, DomainEventEnvelope> connectivityMatrixEventResult = drainTableOutput(consumerConnectivityMatrixEvent);
//
//            // then
//            assertThat(connectivityMatrixEventResult.keySet())
//                    .isEqualTo(EXPECTED_CONNECTIVITY_MATRIX_EVENT_MAP.keySet());
//            assertThat(connectivityMatrixEventResult).isEqualToComparingFieldByFieldRecursively(EXPECTED_CONNECTIVITY_MATRIX_EVENT_MAP);
//
//        }
//    }
//
//    private ConfigurableApplicationContext startApplication() {
//        SpringApplication app = new SpringApplication(DataForwarderApplication.class);
//        app.setWebApplicationType(WebApplicationType.NONE);
//        return app.run(
//                "--server.port=0",
//                "--spring.jmx.enabled=false",
//                "--spring.cloud.stream.function.definition=tacticalModelEventConsumer;tacticalModelEventSupplier;connectivityMatrixEventConsumer;",
//                "--spring.cloud.stream.bindings.tacticalModelEventConsumer-in-0.destination=" + TACTICAL_MODEL_EVENT_CONSUMER,
//                "--spring.cloud.stream.bindings.connectivityMatrixEventConsumer-in-0.destination" + CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                "--spring.cloud.stream.bindings.tacticalModelEventSupplier-out-0.destination=" + TACTICAL_MODEL_EVENT_SUPPLIER,
//                "--spring.cloud.stream.kafka.streams.bindings.connectivityMatrixEventConsumer-in-0.consumer.application-id=connectivity-matrix-input-df-kafka-id-test",
//                "--spring.cloud.stream.kafka.streams.bindings.tacticalModelEventConsumer-in-0.consumer.application-id=tactical-model-event-consumer-input-df-kafka-id-test",
//                "--spring.cloud.stream.kafka.streams.binder.brokers=" + embeddedKafka.getBrokersAsString(),
//                "--spring.cloud.stream.kafka.streams.binder.configuration.commit.interval.ms=1",
//                "--spring.cloud.stream.kafka.binder.brokers=" + embeddedKafka.getBrokersAsString(),
//                "--spring.kafka.producer.bootstrap-servers=" + embeddedKafka.getBrokersAsString()
//
//        );
//    }
//}
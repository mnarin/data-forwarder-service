//package tr.com.havelsan.kkst.tdlr;
//
//import org.junit.jupiter.api.AfterEach;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.cloud.contract.stubrunner.StubFinder;
//import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
//import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
//import org.springframework.cloud.contract.verifier.messaging.MessageVerifier;
//import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
//import org.springframework.context.annotation.Import;
//import org.springframework.kafka.test.EmbeddedKafkaBroker;
//import org.springframework.kafka.test.context.EmbeddedKafka;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//import tr.com.havelsan.kkss.kafka.contract.CustomContractVerifierKafkaConfiguration;
//import tr.com.havelsan.kkss.kafka.contract.SpringCloudContractTestUtil;
//import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
//
//import java.util.Map;
//import java.util.Optional;
//
//import static tr.com.havelsan.kkss.kafka.contract.SpringCloudContractTestUtil.createPayloadIncludesHeaders;
//import static tr.com.havelsan.kkss.kafka.contract.SpringCloudContractTestUtil.getContractBodyData;
//
//
//@SpringBootTest(classes = DataForwarderApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE,
//        properties = {
//                "server.port=0",
//                "spring.jmx.enabled=false",
//                "spring.kafka.bootstrap-servers=127.0.0.1:9093",
//                "spring.kafka.producer.bootstrap-servers=127.0.0.1:9093",
//                "spring.kafka.consumer.key-deserializer=org.apache.kafka.common.serialization.IntegerDeserializer",
//                "spring.kafka.consumer.value-deserializer=org.apache.kafka.common.serialization.StringDeserializer",
//                "spring.kafka.producer.key-serializer=org.apache.kafka.common.serialization.IntegerSerializer",
//                "spring.kafka.producer.value-serializer=org.apache.kafka.common.serialization.StringSerializer",
//                "stubrunner.kafka.enabled=true",
//                "stubrunner.stream.enabled=false",
//                "stubrunner.integration.enabled=false",
//        }
//)
//@AutoConfigureMessageVerifier
//@SpringJUnitConfig
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//@EmbeddedKafka(
//        partitions = 1,
//        topics = {
//                ContractMessagingBase.TACTICAL_MODEL_TOPIC,
//                ContractMessagingBase.CONNECTIVITY_MATRIX_TOPIC
//        },
//        brokerProperties = {
//                "port=9093",
//                "listeners=PLAINTEXT://127.0.0.1:9093"
//        }
//)
//@AutoConfigureStubRunner(
//        stubsMode = StubRunnerProperties.StubsMode.LOCAL,
//        ids = "${stubrunner.ids}"
//)
//@Import({CustomContractVerifierKafkaConfiguration.class})
//public abstract class ContractMessagingBase {
//    static final String TACTICAL_MODEL_TOPIC = "tactical-model-topic";
//    static final String CONNECTIVITY_MATRIX_TOPIC = "connectivity-matrix-topic";
//
//    @Autowired
//    StubFinder stubFinder;
//
//    @Qualifier("contractVerifierKafkaMessageExchange") // If stubrunner.kafka.enabled=true
//    @Autowired
//    MessageVerifier<?> messaging;
//
//    void tacticalModelEvent() {
//        sendEventDataInContract("tacticalModelEvent", TACTICAL_MODEL_TOPIC);
//    }
//
//    private void sendEventDataInContract(String contractLabel, String destinationTopic) {
//        DomainEventEnvelope<?> domainEventEnvelope = Optional.of(contractLabel)
//                .map(label -> getContractBodyData(stubFinder, label, DomainEventEnvelope.class))
//                .orElse(null);
//
//        Map<String, Object> headers = Optional.ofNullable(domainEventEnvelope)
//                .map(DomainEventEnvelope::getKey)
//                .map(Integer::parseInt)
//                .map(SpringCloudContractTestUtil::createHeaders)
//                .orElse(null);
//
//        Optional.ofNullable(headers)
//                .map(headersMap -> createPayloadIncludesHeaders(domainEventEnvelope, headersMap))
//                .ifPresent(payload -> messaging.send(payload, headers, destinationTopic));
//    }
//}

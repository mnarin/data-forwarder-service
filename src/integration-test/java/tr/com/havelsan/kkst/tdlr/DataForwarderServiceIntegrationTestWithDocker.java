//package tr.com.havelsan.kkst.tdlr;
//
//
//import io.confluent.kafka.serializers.KafkaAvroDeserializer;
//import org.apache.kafka.clients.consumer.Consumer;
//import org.apache.kafka.common.serialization.Serdes;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.WebApplicationType;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.serializer.JsonSerde;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//import org.testcontainers.containers.GenericContainer;
//import org.testcontainers.containers.KafkaContainer;
//import org.testcontainers.containers.Network;
//import org.testcontainers.junit.jupiter.Testcontainers;
//import org.testcontainers.utility.DockerImageName;
//import tr.com.havelsan.kkst.tdlr.datamodel.base.event.DomainEventEnvelope;
//
//import java.lang.reflect.Field;
//import java.util.Map;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static tr.com.havelsan.kkss.kafka.CustomSpringKafkaTestUtils.*;
//import static tr.com.havelsan.kkst.tdlr.DataForwardingServiceSampleDatas.*;
//
//@SpringJUnitConfig
//@Testcontainers
//public class DataForwarderServiceIntegrationTestWithDocker {
//
//    private static final DockerImageName KAFKA_TEST_IMAGE = DockerImageName.
//            parse("cregistry.k2.net/library/confluentinc/cp-kafka:5.2.2")
//            .asCompatibleSubstituteFor("confluentinc/cp-kafka");
//    private static final DockerImageName SCHEMA_REGISTRY_IMAGE = DockerImageName
//            .parse("cregistry.k2.net/library/confluentinc/cp-schema-registry:5.4.0")
//            .asCompatibleSubstituteFor("confluentinc/cp-schema-registry");
//
//    static final String TACTICAL_MODEL_EVENT_CONSUMER = "tactical-model-topic";
//    static final String CONNECTIVITY_MATRIX_EVENT_CONSUMER = "connectivity-matrix-topic";
//    static final String TACTICAL_MODEL_EVENT_SUPPLIER = "tactical-model-topic";
//    static final String TACTICAL_MODEL_EVENT_AVRO_SUPPLIER = "tactical-model-avro-topic";
//
//    static {
//        setRyukDisabledInEnv();
//    }
//
//    private Consumer<Integer, DomainEventEnvelope> consumerTacticalModelEvent;
//    private Consumer<Object, Object> consumerTacticalModelAvroEvent;
//    private Consumer<Integer, DomainEventEnvelope> consumerConnectivityMatrixEvent;
//    private KafkaTemplate<Integer, DomainEventEnvelope> tacticalModelEventKafkaTemplate;
//
//    private KafkaTemplate<Integer, DomainEventEnvelope> connectivityMatrixEventKafkaTemplate;
//    private GenericContainer<?> schemaRegistry;
//    private KafkaContainer kafka;
//    private Network network;
//
//
//    @BeforeEach
//    public void setup() {
//        network = Network.newNetwork();
//        kafka = new KafkaContainer(KAFKA_TEST_IMAGE)
//                .withNetwork(network);
//
//        schemaRegistry = new GenericContainer<>(SCHEMA_REGISTRY_IMAGE)
//                .withExposedPorts(9081)
//                .withNetwork(network)
//                .withEnv("SCHEMA_REGISTRY_HOST_NAME", "schema-registry")
//                .withEnv("SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS", "PLAINTEXT://" + kafka.getNetworkAliases().get(0) + ":9092")
//                .withEnv("SCHEMA_REGISTRY_LISTENERS", "http://0.0.0.0:9081");
//        kafka.start();
//        schemaRegistry.start();
//
//        connectivityMatrixEventKafkaTemplate = producer(
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                kafka.getBootstrapServers()
//        );
//        tacticalModelEventKafkaTemplate = producer(
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                kafka.getBootstrapServers()
//        );
//
//        consumerTacticalModelEvent = consumer(
//                TACTICAL_MODEL_EVENT_CONSUMER,
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                kafka.getBootstrapServers()
//        );
//        consumerTacticalModelAvroEvent = consumer(
//                TACTICAL_MODEL_EVENT_AVRO_SUPPLIER,
//                Serdes.Integer(),
//                KafkaAvroDeserializer.class,
//                kafka.getBootstrapServers(),
//                getSchemaRegistertyUrl()
//        );
//
//        consumerConnectivityMatrixEvent = consumer(
//                CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                Serdes.Integer(),
//                new JsonSerde<>(DomainEventEnvelope.class),
//                kafka.getBootstrapServers()
//        );
//    }
//
//    @AfterEach
//    public void tearDown() {
//        kafka.close();
//        schemaRegistry.close();
//        network.close();
//        close(consumerTacticalModelEvent);
//        close(consumerConnectivityMatrixEvent);
//
//    }
//
//
//    @Test
//    public void tacticalModelEventConsumerTestWithoutConnectivityMatrix() throws Exception {
//
//        //given
//        try (ConfigurableApplicationContext ignored = startApplication()) {
//            // when
//            produceKeyValuesSynchronously(
//                    tacticalModelEventKafkaTemplate,
//                    TACTICAL_MODEL_EVENT_CONSUMER,
//                    TACTICAL_MODEL_EVENT_LIST_1.stream()
//            );
//
//            Map<Integer, DomainEventEnvelope> tacticalModelEventResult = drainTableOutput(consumerTacticalModelEvent);
//
//            // then
//            assertThat(tacticalModelEventResult.keySet())
//                    .isEqualTo(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FALSE.keySet());
//            assertThat(tacticalModelEventResult).isEqualToComparingFieldByFieldRecursively(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_FALSE);
//        }
//    }
//
//    @Test
//    public void tacticalModelEventAvroConsumerTestWithConnectivityMatrix() throws InterruptedException {
//
//        //given
//        try (ConfigurableApplicationContext ignored = startApplication()) {
//            produceKeyValuesSynchronously(
//                    connectivityMatrixEventKafkaTemplate,
//                    CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                    CONNECTIVITY_MATRIX_EVENT_LIST_1.stream()
//            );
//            Thread.sleep(5000L);
//            // when
//            produceKeyValuesSynchronously(
//                    tacticalModelEventKafkaTemplate,
//                    TACTICAL_MODEL_EVENT_CONSUMER,
//                    TACTICAL_MODEL_EVENT_LIST_1.stream()
//            );
//            Thread.sleep(5000L);
//            Map<Object, Object> tacticalModelEventResult = drainTableOutput(consumerTacticalModelAvroEvent);
//
//            // then
//            assertThat(tacticalModelEventResult.keySet())
//                    .isEqualTo(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE_FOR_AVTO.keySet());
//            assertThat(tacticalModelEventResult).isEqualToComparingFieldByFieldRecursively(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE_FOR_AVTO);
//        }
//    }
//
//    @Test
//    public void tacticalModelEventConsumerWithConnectivityMatrix() throws InterruptedException {
//
//        //given
//        try (ConfigurableApplicationContext ignored = startApplication()) {
//            produceKeyValuesSynchronously(
//                    connectivityMatrixEventKafkaTemplate,
//                    CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                    CONNECTIVITY_MATRIX_EVENT_LIST_1.stream()
//            );
//            Thread.sleep(5000L);
//            // when
//            produceKeyValuesSynchronously(
//                    tacticalModelEventKafkaTemplate,
//                    TACTICAL_MODEL_EVENT_CONSUMER,
//                    TACTICAL_MODEL_EVENT_LIST_1.stream()
//            );
//            Thread.sleep(5000L);
//            Map<Integer, DomainEventEnvelope> tacticalModelEventResult = drainTableOutput(consumerTacticalModelEvent);
//
//            // then
//            assertThat(tacticalModelEventResult.keySet())
//                    .isEqualTo(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE.keySet());
//            assertThat(tacticalModelEventResult).isEqualToComparingFieldByFieldRecursively(EXPECTED_TACTICAL_MODEL_EVENT_MAP_WITH_TRUE);
//        }
//    }
//
//    @Test
//    public void connectivityMatrixEventConsumerTest() {
//        try (ConfigurableApplicationContext ignored = startApplication()) {
//            // when
//            produceKeyValuesSynchronously(
//                    connectivityMatrixEventKafkaTemplate,
//                    CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                    CONNECTIVITY_MATRIX_EVENT_LIST_1.stream()
//            );
//            Map<Integer, DomainEventEnvelope> connectivityMatrixEventResult = drainTableOutput(consumerConnectivityMatrixEvent);
//
//            // then
//            assertThat(connectivityMatrixEventResult.keySet())
//                    .isEqualTo(EXPECTED_CONNECTIVITY_MATRIX_EVENT_MAP.keySet());
//            assertThat(connectivityMatrixEventResult).isEqualToComparingFieldByFieldRecursively(EXPECTED_CONNECTIVITY_MATRIX_EVENT_MAP);
//        }
//    }
//
//    private String getSchemaRegistertyUrl() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("http://").append(schemaRegistry.getContainerIpAddress());
//        sb.append(":").append(schemaRegistry.getMappedPort(9081));
//        return sb.toString();
//    }
//
//    private static void setRyukDisabledInEnv() {
//        try {
//            Class<?> processEnvironmentClass = Class.forName("java.lang.ProcessEnvironment");
//            Field theCaseInsensitiveEnvironment = processEnvironmentClass.getDeclaredField("theCaseInsensitiveEnvironment");
//            theCaseInsensitiveEnvironment.setAccessible(true);
//            Map<String, String> caseEnv = (Map<String, String>) theCaseInsensitiveEnvironment.get(null);
//            caseEnv.put("TESTCONTAINERS_RYUK_DISABLED", "true");
//        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private ConfigurableApplicationContext startApplication() {
//        SpringApplication app = new SpringApplication(DataForwarderApplication.class);
//        app.setWebApplicationType(WebApplicationType.NONE);
//        return app.run(
//                "--server.port=0",
//                "--spring.jmx.enabled=false",
//                "--spring.cloud.stream.function.definition=tacticalModelEventConsumer;tacticalModelEventSupplier;connectivityMatrixEventConsumer;tacticalModelEventAvroSupplier;",
//                "--spring.cloud.stream.bindings.tacticalModelEventConsumer-in-0.destination=" + TACTICAL_MODEL_EVENT_CONSUMER,
//                "--spring.cloud.stream.bindings.connectivityMatrixEventConsumer-in-0.destination" + CONNECTIVITY_MATRIX_EVENT_CONSUMER,
//                "--spring.cloud.stream.bindings.tacticalModelEventSupplier-out-0.destination=" + TACTICAL_MODEL_EVENT_SUPPLIER,
//                "--spring.cloud.stream.bindings.tacticalModelEventAvroSupplier-out-0.destination=" + TACTICAL_MODEL_EVENT_AVRO_SUPPLIER,
//                "--spring.cloud.stream.bindings.tacticalModelEventAvroSupplier-out-0.contentType=application/vnd.user.v1+avro",
//                "--spring.cloud.stream.bindings.tacticalModelEventAvroSupplier-out-0.contentType.producer.useNativeEncoding=true",
//                "--spring.cloud.stream.kafka.streams.bindings.tacticalModelEventConsumer-in-0.consumer.application-id=tactical-model-event-consumer-input-df-id-test",
//                "--spring.cloud.stream.kafka.streams.bindings.connectivityMatrixEventConsumer-in-0.consumer.application-id=connectivity-matrix-input-df-id-test",
//                "--spring.cloud.stream.kafka.streams.binder.brokers=" + kafka.getBootstrapServers(),
//                "--spring.cloud.stream.kafka.streams.binder.configuration.commit.interval.ms=1",
//                "--spring.cloud.stream.kafka.streams.binder.configuration.schema.registry.url=" + getSchemaRegistertyUrl(),
//                "--spring.cloud.stream.kafka.binder.producer-properties.schema.registry.url=" + getSchemaRegistertyUrl(),
//                "--spring.cloud.stream.kafka.bindings.tacticalModelEventAvroSupplier-out-0.producer.configuration.value.serializer=tr.com.havelsan.kkss.kafka.CustomAvroSerializer",
//                "--spring.cloud.stream.kafka.binder.brokers=" + kafka.getBootstrapServers(),
//                "--spring.kafka.producer.bootstrap-servers=" + kafka.getBootstrapServers()
//        );
//    }
//}

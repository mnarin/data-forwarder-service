package contracts

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    label 'tacticalModelEvent'
    input {
        triggeredBy('tacticalModelEvent()')
    }

    outputMessage {
        sentTo('tactical-model-topic')
        body(
                '''{
                    "type":"DomainEventEnvelopeImpl",
                    "key":"1",
                    "eventType":"TACTICAL_MODEL_CALCULATION_EVENT",
                    "event":{
                        "type":"TacticalModelEvent",
                        "resourceId":1,
                        "baseTacticalModel":{
                            "type":"AirTrack",
                            "entityKey":{
                                "key":"1834568837",
                                "resourceId":-2000000000,
                                "resourceTypesEnum":null
                                },
                            "sourceTN":-2000000000,
                            "linkTN":-2000000000,
                            "trackCharacteristics":{
                                "exerciseIndicator":false,
                                "ppliTrackNoAndIdIndicator":false,
                                "forceTellIndicator":false,
                                "emergencyIndicator":false,
                                "specialProcessingIndicator":false,
                                "simulationIndicator":false,
                                "identityDiffIndicator":false
                                },
                            "identity":"PENDING",
                            "identityAmpDesc":"EXERCISE_PENDING",
                            "initiationTime":-9000000000000000000,
                            "lastUpdateTime":-9000000000000000000,
                            "trackPliIndicator":"TRACK",
                            "characteristics":{
                                "exerciseIndicator":false,
                                "ppliTrackNoAndIdIndicator":false,
                                "forceTellIndicator":false,
                                "emergencyIndicator":false,
                                "specialProcessingIndicator":false,
                                "simulationIndicator":false,
                                "identityDiffIndicator":false,
                                "specialInterestIndicator":false,
                                "strength":-2000000000
                                },
                            "geoInformation":{
                                "latitude":1.0E-4,
                                "longitude":1.0E-4,
                                "course":1.0E-4,
                                "speed":1.0E-4,
                                "geodeticPositionQuality":-2000000000,
                                "posUpdateTime":-9000000000000000000,
                                "altitudeSource":-2000000000,
                                "altitude":1.0E-4,
                                "trackQuality":-2000000000,
                                "passiveActiveIndicator":false
                                },
                            "airTrackIdentityInformation":{
                                "mode1Code":-2000000000,
                                "mode2Code":-2000000000,
                                "mode3Code":-2000000000,
                                "mode4Indicator":-2000000000,
                                "minute":-2000000000,
                                "hour":-2000000000,
                                "ppliIffSifIndicator":-2000000000,
                                "airSpecificTypeIndicator":false,
                                "airSpecificType":-2000000000,
                                "airPlatform":-2000000000,
                                "airActivity":-2000000000,
                                "callSign":""
                                }
                             },
                         "remoteLocalIndicator":"LOCAL"
                       }
                    }'''
        )
        headers {
            header('kafka_messageKey', 1)
            messagingContentType(applicationJson())

        }
    }
}